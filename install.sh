#!/usr/bin/env bash
set -euxo pipefail

ORIG_DIR=$PWD
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# set working dir
cd $DIR

# TODO: detect conflicting config files in home dir

# ensure env vars are set for use during install
. $DIR/config/zsh/.zshenv

# setup symlinks to config dirs
stow --restow -v --target $XDG_CONFIG_HOME config

# setup zsh init file
ln -sf $DIR/config/zsh/init.zsh $HOME/.zshenv

# download zsh themes
ZSH_THEMES_DIR=$XDG_DATA_HOME/zsh/themes
mkdir -p $ZSH_THEMES_DIR
wget -O $ZSH_THEMES_DIR/minimal.zsh https://raw.githubusercontent.com/subnixr/minimal/master/minimal.zsh
if [ ! -d $ZSH_THEMES_DIR/powerlevel10k ]; then
    git clone --depth=1 https://gitee.com/romkatv/powerlevel10k.git $ZSH_THEMES_DIR/powerlevel10k
fi

# install tmux plugin manager
TMUX_PLUGINS_DIR=$XDG_CONFIG_HOME/tmux/plugins
mkdir -p $TMUX_PLUGINS_DIR
if [ ! -d $TMUX_PLUGINS_DIR/tpm ]; then
    git clone https://github.com/tmux-plugins/tpm $TMUX_PLUGINS_DIR/tpm
fi

# install tldr
curl -o $XDG_BIN_HOME/tldr https://raw.githubusercontent.com/raylee/tldr/master/tldr
chmod +x $XDG_BIN_HOME/tldr

# install cht.sh
curl https://cht.sh/:cht.sh > $XDG_BIN_HOME/cht.sh
chmod +x $XDG_BIN_HOME/cht.sh
CHTSH_DATA_DIR=$XDG_DATA_HOME/cht.sh
mkdir -p $CHTSH_DATA_DIR
curl https://cheat.sh/:bash_completion > $CHTSH_DATA_DIR/bash_completion

# download asdf
if [ ! -d $ASDF_DIR ]; then
    git clone https://github.com/asdf-vm/asdf.git $ASDF_DIR --branch v0.14.0
fi

# link global tool version files
ln -sf $XDG_CONFIG_HOME/tool-versions $HOME/.tool-versions
ln -sf $XDG_CONFIG_HOME/envrc $HOME/.envrc

cd $ORIG_DIR
